#include<iostream>
#include<cstdlib>
#include<string>
#include"bintree.h"
#include"useful.h"
using namespace std;

void ask_and_move(binary_tree_node<string>*& current_ptr);
binary_tree_node<string>* beginning_tree();
void instruct();
void learn(binary_tree_node<string>*& leaf_ptr);
void play(binary_tree_node<string>* current_ptr);

int main() {
	binary_tree_node<string>* animal_root_ptr;
	instruct();
	animal_root_ptr = beginning_tree();
	do
		play(animal_root_ptr);
	while (inquire("还要不要再玩一次？"));
	cout << "欢迎下次来玩~" << endl;
	return EXIT_SUCCESS;
	while (1);
}
void instruct() {
	cout << "现在开始游戏吧" << endl;
	cout << "你脑海中想象一只动物，让我来猜一猜" << endl;
}
void ask_and_move(binary_tree_node<string>*& current_ptr) {
	cout << current_ptr->data();//输出问题
	if (inquire("请回答:")) {
		current_ptr = current_ptr->left();
	}
	else
		current_ptr = current_ptr->right();
}
//创建二叉树
binary_tree_node<string>* beginning_tree(){
	binary_tree_node<string>* root_ptr;//头节点指针
	binary_tree_node<string>* child_ptr;//孩子节点指针
	const string root_question("你是哺乳动物么？");
	const string left_question("你的体型比猫大，对么？");
	const string right_question("这个动物是生活在水里么？");
	const string animal_1("袋鼠");
	const string animal_2("老鼠");
	const string animal_3("罗非鱼");
	const string animal_4("喜鹊");

	//根节点
	root_ptr = new binary_tree_node<string>(root_question);//头节点指针指向新创建的节点问题

	//左子树
	child_ptr = new binary_tree_node<string>(left_question);//左孩子问题
	child_ptr->set_left(new binary_tree_node<string>(animal_1));
	child_ptr->set_right(new binary_tree_node<string>(animal_2));

	root_ptr->set_left(child_ptr);//将根节点的左节点设置为子节点的左节点

	//右子树
	child_ptr = new binary_tree_node<string>(right_question);
	child_ptr->set_left(new binary_tree_node<string>(animal_3));
	child_ptr->set_right(new binary_tree_node<string>(animal_4));

	root_ptr->set_right(child_ptr);//将根节点的右节点设置为子节点的右节点
	return root_ptr;
}
void learn(binary_tree_node<string>*& leaf_ptr) {
	string guess_animal;
	string correct_animal;
	string new_question;
	guess_animal = leaf_ptr->data();
	cout << "好吧，我不知道是什么，还是你告诉我吧?" << endl;
	getline(cin, correct_animal);
	cout << "现在请输入一个新的问题来区分 ";
	cout <<'['<<correct_animal<<']'<< " 和 " << '['<<guess_animal<<']' << endl;
	getline(cin, new_question);
	leaf_ptr->set_data(new_question);
	cout<<correct_animal << "," << new_question << endl;
	if (inquire("请回答")) {//如果回答的是正确的则做节点
		leaf_ptr->set_left(new binary_tree_node<string>(correct_animal));
		leaf_ptr->set_right(new binary_tree_node<string>(guess_animal));
	}
	else {
		leaf_ptr->set_left(new binary_tree_node<string>(guess_animal));
		leaf_ptr->set_right(new binary_tree_node<string>(correct_animal));
	}
}
void play(binary_tree_node<string>* current_ptr) {
	cout << "你现在脑海里想象一只动物，想好了然后按下回车.";
	eat_line();//等待，直到按下回车
	while (!current_ptr->is_leaf())//如果不是叶子节点就执行ask_and_remove（）函数，因为是叶子节点就直接输出最后猜测的结果
		ask_and_move(current_ptr);
	cout << ("是" + current_ptr->data()+"吧");
	if (!inquire(",我猜对了吗？"))//询问是否猜对，否，将该动物添加到二叉树中
		learn(current_ptr);
	else
		cout << "我猜对了，哈哈哈哈" << endl;

}